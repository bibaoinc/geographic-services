package com.bibao.geographic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bibao.geographic.config.GeographicServicesConfig;

@SpringBootApplication
@Import(GeographicServicesConfig.class)
public class GeographicServicesApp {
	public static void main(String[] args) {
		SpringApplication.run(GeographicServicesApp.class, args);
	}
}
