package com.bibao.geographic.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.bibao.geographic")
public class GeographicServicesConfig {

}
